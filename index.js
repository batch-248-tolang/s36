// setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

// this allows us to use all the routes defined in taskRoutes.js
const taskRoutes = require("./routes/taskRoutes");

// server setup
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// database collection
// connecting to mongodb atlas

mongoose.set("strictQuery", true);

mongoose.connect("mongodb+srv://admin:admin123@clusterbatch248.hg26zix.mongodb.net/s35?retryWrites=true&w=majority",{

	useNewUrlParser: true,
	useUnifiedTopology: true
});

// connection to database
let db = mongoose.connection;

db.on("error",console.error.bind(console,"connection error"));
db.once("open",()=>console.log("Hi! We are connected to MongoDB Atlas"))


// add task route
// allows all the task routes created in the taskRoutes.js file to use "/tasks" route
app.use("/tasks",taskRoutes)


// server listening
app.listen(port,()=>console.log(`Now listening to ${port}`))